﻿using BUS;
using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        private readonly BookService bookservice = new BookService();
        private readonly CategoryService categoryservice = new CategoryService();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var listCategory = categoryservice.GetAll();
            var listbook = bookservice.GetAll();
            BinGrid(listbook);
            FillCategoryCombobox(listCategory);
        }


        private void FillCategoryCombobox(List<CatagoryBook> listCategory)
        {
            listCategory.Insert(3, new CatagoryBook());
            this.cbCategory.DataSource = listCategory;
            this.cbCategory.DisplayMember = "CategoryName";
            this.cbCategory.ValueMember = "CategoryID";
        }


        private void BinGrid(List<Book> books)
        {
            dgvBook.Rows.Clear();
            foreach (var item in books)
            {
                int index = dgvBook.Rows.Add(item);
                dgvBook.Rows[index].Cells[0].Value = item.BookID;
                dgvBook.Rows[index].Cells[1].Value = item.BookName;
                dgvBook.Rows[index].Cells[2].Value = item.YearOfPublished;
                dgvBook.Rows[index].Cells[3].Value = item.CatagoryBook.CategoryName;
            }
        }



        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in dgvBook.Rows)
                {
                    if (row.Cells[0].Value != null && row.Cells[0].Value.ToString() == txtBookID.Text)
                    {
                        MessageBox.Show("BookID already exists, please enter a different ID.");
                        return;
                    }
                }

                if (string.IsNullOrEmpty(txtBookID.Text) || string.IsNullOrEmpty(txtPublishing.Text) || string.IsNullOrEmpty(txttNameBook.Text))
                {
                    MessageBox.Show("Please enter complete information");
                }
                else if (txtBookID.TextLength != 6)
                {
                    MessageBox.Show("Book id must have 6 characters");
                }
                else
                {
                    Book b = new Book()
                    {
                        BookID = txtBookID.Text,
                        BookName = txttNameBook.Text,
                        YearOfPublished = int.Parse(txtPublishing.Text),
                        CatagoryID = (int)cbCategory.SelectedValue
                    };
                    bookservice.AddOrUpdate(b);
                    MessageBox.Show("Add successfully");
                    BinGrid(bookservice.GetAll());
                    txtBookID.Clear();
                    txttNameBook.Clear();
                    txtPublishing.Clear();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void dgvBook_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvBook.Rows[e.RowIndex];
                txtBookID.Text = row.Cells[0].Value.ToString();
                txttNameBook.Text = row.Cells[1].Value.ToString();
                txtPublishing.Text = row.Cells[2].Value.ToString();
                cbCategory.Text = row.Cells[3].Value.ToString();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string bookIDToDelete = txtBookID.Text;
                bool deleted = bookservice.DeleteBookByBookID(bookIDToDelete);

                if (deleted)
                {
                    MessageBox.Show("Book deleted successfully");
                    BinGrid(bookservice.GetAll());
                    txtBookID.Clear();
                    txttNameBook.Clear();
                    txtPublishing.Clear();
                }
                else
                {
                    MessageBox.Show("Book not found or could not be deleted");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Error");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string updateID = txtBookID.Text;
                Book update = bookservice.GetBookByBookID(updateID);
                if (update != null)
                {
                    update.BookName = txttNameBook.Text;
                    update.YearOfPublished = int.Parse(txtPublishing.Text);
                    update.CatagoryID = (int)cbCategory.SelectedValue;
                    bookservice.AddOrUpdate(update);
                    MessageBox.Show("Update successfully");

                    BinGrid(bookservice.GetAll());
                    txtBookID.Clear();
                    txttNameBook.Clear();
                    txtPublishing.Clear();

                }
                else
                {
                    MessageBox.Show("Book Not found or could not be update");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Error");
            }
        }

        private void txtFind_TextChanged(object sender, EventArgs e)
        {
            string find = txtFind.Text;
            if (string.IsNullOrEmpty(find))
            {
                BinGrid(bookservice.GetAll());
            }
            else
            {
                BinGrid(bookservice.Search(find));
            }
        }

        private void statisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reportviwer reportviwer = new reportviwer();
            reportviwer.ShowDialog();
        }
    }


}

