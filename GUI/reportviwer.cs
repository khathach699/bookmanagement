﻿using DAL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class reportviwer : Form
    {
        public reportviwer()
        {
            InitializeComponent();
        }

        private void reportviwer_Load(object sender, EventArgs e)
        {
            BookModel context = new BookModel();
            List<Book> booklist = context.Books.OrderByDescending(b => b.YearOfPublished).ToList();

            List<bookrp> bookrp = new List<bookrp>();
            foreach (Book b in booklist)
            {
                bookrp temp = new bookrp();
                temp.YearOfpublished = b.YearOfPublished.ToString();
                temp.BookID = b.BookID;
                temp.BookName = b.BookName;
                temp.category = b.CatagoryBook.CategoryName.ToString();
                bookrp.Add(temp);
            }

            reportViewer1.LocalReport.ReportPath = "rpBook.rdlc";
            var sour = new ReportDataSource("bookdata", bookrp);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(sour);
            this.reportViewer1.RefreshReport();

        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
