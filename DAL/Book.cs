namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Book")]
    public partial class Book
    {
        [StringLength(6)]
        public string BookID { get; set; }

        [Required]
        [StringLength(200)]
        public string BookName { get; set; }

        public int YearOfPublished { get; set; }

        public int CatagoryID { get; set; }

        public virtual CatagoryBook CatagoryBook { get; set; }
    }
}
