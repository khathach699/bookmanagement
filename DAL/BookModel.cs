using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace DAL
{
    public partial class BookModel : DbContext
    {
        public BookModel()
            : base("name=BookModel")
        {
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<CatagoryBook> CatagoryBooks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .Property(e => e.BookID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CatagoryBook>()
                .HasMany(e => e.Books)
                .WithRequired(e => e.CatagoryBook)
                .HasForeignKey(e => e.CatagoryID)
                .WillCascadeOnDelete(false);
        }
    }
}
