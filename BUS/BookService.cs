﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BookService
    {
        private BookModel context;

        public BookService()
        {
            context = new BookModel();
        }

        public List<Book> GetAll()
        {
            return context.Books.ToList();
        }

        public void AddOrUpdate(Book b)
        {
            context.Books.AddOrUpdate(b);
            context.SaveChanges();
        }
        public bool DeleteBookByBookID(string bookID)
        {
            Book bookToDelete = GetBookByBookID(bookID);
            if (bookToDelete != null)
            {
                context.Books.Remove(bookToDelete);
                context.SaveChanges();
                return true; // Trả về true nếu xóa thành công
            }
            return false;
        }

        public Book GetBookByBookID(string bookID)
        {
            return context.Books.FirstOrDefault(p => p.BookID == bookID);
        }

        public List<Book> Search(string keyword)
        {
            return context.Books.Where(b => b.BookID.Contains(keyword)
                                        || b.BookName.Contains(keyword)
                                        || b.CatagoryBook.CategoryName.Contains(keyword)
                                        || b.YearOfPublished.ToString() == (keyword)).ToList();
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
